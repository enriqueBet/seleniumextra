from driver_extensions import ExtendedFirefox

def main():
    url = "https://www.python.org/jobs/location/telecommute/"
    firefox = ExtendedFirefox()
    firefox.get(url)
    assert "Python Job Board" in firefox.title
    firefox.getByXpath("/html/body/div[1]/div[3]/div/section/div/ol/li[1]/h2/span[1]/a","click()")

if __name__ == "__main__":
    main()

