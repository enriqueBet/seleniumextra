from selenium import webdriver
from selenium.common.exceptions import *
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from time import sleep

class ExtendedFirefox(webdriver.Firefox):
    def __init__(self,profile = {}*args,**kwargs):
        # Initialize the firefox profile
        firefox_profile = webdriverFirefoxProfile()
        for k, v in profile.items():
            profile.set_preference(k,v)
        super(ExtendedFirefox,self).__init__(*args,**kwargs)
        self.retries = 3
        self.retry_time = 2
        self.wait_time = 10

    def __getByFunc__(self,func,query,action=None):
        done = False
        retries = 0
        while not done:
            if retries > self.retries:
                raise Exception(f"[ERROR] Can't find element {query} after retrying {self.retries} times.")
                break
            try:
                element = WebDriverWait(self,self.wait_time).until(EC.presence_of_element_located((func,query,action)))
                if action != None:
                    element = eval(f"element.{action}")
                done = True
            except Exception as e:
                print(e)
                retries += 1
            sleep(self.retry_time)
        return element

    def __existBy__(self,func,query):
        try:
            element = self.__getByFunc__(func,query)
            return True
        else:
            return False

    # Wrappers for the __getByFunc__ function
    def getByXpath(self,query,action=None):
        return self.__getByFunc__(By.XPATH,query,action)

    def getByCSS(self,query,action=None):
        return self.__getByFunc__(By.CSS_SELECTOR,query,action)

    def getByID(self,query,action=None):
        return self.__getByFunc__(By.ID,query,action)

    def getByClass(self,query,action=None):
        return self.__getByFunc__(By.CLASS_NAME,query,action)

    def getByName(self,query,action=None):
        return self.__getByFunc__(By.NAME,query,action)

    def getByLink(self,query,action=None):
        return self.__getByFunc__(By.LINK_TEST,query,action)

    def getByPartLink(self,query,action=None):
        return self.__getByFunc__(By.PARTIAL_LINK_TEXT,query,action)

    def getByTAG(self,query,action=None):
        return self.__getByFunc__(By.TAG_NAME,query,action)

    # Wrappers for __existByFunc__
    def existXpath(self,query):
        return self.__existBy__(By.XPATH,query)

    def existCSS(self,query):
        return self.__existBy__(By.CSS_SELECTOR,query)

    def existID(self,query):
        return self.__existBy__(By.ID,query)

    def existClass(self,query):
        return self.__existBy__(By.CLASS_NAME,query)

    def existName(self,query):
        return self.__existBy__(By.NAME,query)

    def existLink(self,query):
        return self.__existBy__(By.LINK_TEST,query)

    def existPartLink(self,query):
        return self.__existBy__(By.PARTIAL_LINK_TEXT,query)

    def existTAG(self,query):
        return self.__existBy__(By.TAG_NAME,query)

    # Configuration methods
    def setWaitTime(self,new_time):
        '''
        Sets the wait time to find a web element
        '''
        self.wait_time = new_time

    def setNumOfRetries(self,new_retry):
        '''
        Sets the number of retries to attempt in order to find a web element
        '''
        self.retries = new_retry

    def setActionTime(self,new_action_time):
        '''
        Sets the time to wait after each action
        '''
        self.retry_time = new_action_time




